# Organization Short Name

Generate a short name for an Organization.

## Algorithm

```js  
const maxLength = 10;
const specialCharactersRegex = /[^a-zA-Z0-9]/g;

function getShortName(clientName) {
    let shortName = "";

    if (clientName && typeof clientName === "string") {
        const cleanName = clientName
            .replaceAll(specialCharactersRegex, " ")
            .replace(/\s+/g, " ")
            .trim();
        const words = cleanName.split(" ");

        let remainingWordsNb = words.length;
        if (remainingWordsNb > maxLength) {
            remainingWordsNb = maxLength;
        }

        for (const word of words) {
            --remainingWordsNb;

            if (shortName.length >= maxLength) {
                break;
            } else {
                for (const letter of word) {
                //console.log('test:', word, letter, shortName, remainingWordsNb)
                if (shortName.length + remainingWordsNb >= maxLength) {
                    break;
                } else {
                    shortName += letter;
                }
                }
            }
        }
    }

    return shortName;
}
```