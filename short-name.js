const companies = [
    "3Com Corp",
    "Ace Hardware Corporation",
    "Administaff, Inc.",
    "Advantica Restaurant Group, Inc.",
    "Agilent Technologies, Inc.",
    "AK Steel Holding Corporation",
    "Allegheny Energy, Inc.",
    "Allmerica Financial Corporation",
    "American Power Conversion Corporation",
    "American Eagle Outfitters, Inc.",
    "American Standard Companies Inc.",
    "AMR Corporation",
    "Anixter International Inc.",
    "Apple Computer, Inc.",
    "Arch Coal, Inc.",
    "Ashland Inc.",
    "Autoliv, Inc.",
    "Avista Corporation",
    "The Bank of New York Company, Inc.",
    "Baxter International Inc.",
    "Bed Bath & Beyond Inc.",
    "Benchmark Electronics, Inc.",
    "BJ Services Company",
    "Boise Cascade Corporation",
    "Brightpoint, Inc.",
    "Brunswick Corporation",
    "C. H. Robinson Worldwide Inc.",
    "Capital One Financial Corp.",
    "Caterpillar Inc.",
    "Cenex Harvest States Cooperatives",
    "Charles Schwab Corp.",
    "Chubb Corp",
    "Circuit City Stores Inc.",
    "The Clorox Co.",
    "Colgate-Palmolive Co.",
    "Commercial Metals Co.",
    "Comverse Technology Inc.",
    "Consolidated Freightways Corp.",
    "Cooper Cameron Corp.",
    "Countrywide Credit Industries Inc.",
    "CSK Auto Corp.",
    "D.R. Horton Inc.",
    "Deere & Company",
    "Devon Energy Corporation",
    "Dole Food Company, Inc.",
    "Dow Chemical Company",
    "DTE Energy Co.",
    "Dynegy Inc.",
    "Eaton Corporation",
    "Electronic Arts Inc.",
    "Encompass Services Corporation",
    "Enterprise Products Partners L.P.",
    "Estee Lauder Companies Inc.",
    "Fairchild Semiconductor International Inc.",
    "Felcor Lodging Trust Inc.",
    "First National of Nebraska Inc.",
    "Fleetwood Enterprises Inc.",
    "Foamex International Inc",
    "Foster Wheeler Ltd.",
    "Gannett Co., Inc.",
    "General Cable Corporation",
    "Gentek Inc.",
    "Georgia-Pacific Corporation",
    "Goodrich Corporation",
    "GreenPoint Financial Corp.",
    "H.B. Fuller Company",
    "Harris Corp.",
    "Health Management Associates Inc.",
    "Hershey Foods Corp.",
    "Home Depot Inc.",
    "Hovnanian Enterprises Inc.",
    "Idacorp Inc.",
    "IMS Health Inc.",
    "International Paper Co.",
    "Intuit Inc.",
    "J.P. Morgan Chase & Co.",
    "John Hancock Financial Services Inc.",
    "Kellwood Company",
    "KeySpan Corp.",
    "Knight-Ridder Inc.",
    "Lam Research Corporation",
    "Legg Mason Inc.",
    "Levi Strauss & Co.",
    "Liz Claiborne Inc.",
    "LSI Logic Corporation",
    "Magellan Health Services Inc.",
    "Mariner Health Care Inc.",
    "Martin Marietta Materials Inc.",
    "Maxtor Corporation",
    "McDonald's Corporation",
    "MeadWestvaco Corporation",
    "Merrill Lynch & Co. Inc.",
    "MGM Mirage",
    "Mirant Corporation",
    "MPS Group Inc.",
    "National Commerce Financial Corporation",
    "Navistar International Corporation",
    "Newmont Mining Corporation",
    "Nordstrom Inc",
    "Northrop Grumman Corporation",
    "Nvidia Corp",
    "OfficeMax Inc",
    "OM Group Inc",
    "Oshkosh Truck Corp",
    "Paccar Inc",
    "Park Place Entertainment Corp",
    "Pennzoil-Quaker State Company",
    "Pepsi Bottling Group Inc.",
    "Perot Systems Corp",
    "Pharmacia Corp",
    "Phoenix Companies Inc",
    "Pittston Brinks Group",
    "PolyOne Corp",
    "Precision Castparts Corp",
    "Pro-Fac Cooperative Inc.",
    "Prudential Financial Inc.",
    "Qualcomm Inc",
    "Qwest Communications Intl Inc",
    "Reader's Digest Association Inc.",
    "Rent A Center Inc",
    "RoadwayCorp",
    "Ross Stores Inc",
    "Sabre Holdings Corp",
    "Sara Lee Corp",
    "Science Applications Intl. Inc.",
    "Sempra Energy",
    "Shopko Stores Inc",
    "Simon Property Group Inc",
    "Solectron Corp",
    "SouthTrust Corp.",
    "Sports Authority Inc",
    "StanCorp Financial Group Inc",
    "State Street Corp.",
    "Stilwell Financial Inc",
    "Sunoco Inc.",
    "Sysco Corp",
    "Tektronix Inc",
    "Tenneco Automotive Inc.",
    "Textron Inc",
    "TMP Worldwide Inc",
    "Trans World Entertainment Corp.",
    "Trigon Healthcare Inc.",
    "Tyson Foods Inc",
    "Union Pacific Corporation",
    "United Parcel Service Inc",
    "Universal Corporation",
    "URS Corporation",
    "UST Inc",
    "Veritas Software Corporation",
    "Vishay Intertechnology Inc",
    "W.W. Grainger Inc",
    "Walt Disney Co",
    "Weatherford International Inc",
    "WESCO International Inc",
    "Whirlpool Corporation",
    "Wisconsin Energy Corporation",
    "Wyeth",
    "Yellow Corporation"
  ];
  
  const maxLength = 10;
  const specialCharactersRegex = /[^a-zA-Z0-9]/g;
  
  function getShortName(clientName) {
    let shortName = "";
  
    if (clientName && typeof clientName === "string") {
      const cleanName = clientName
        .replaceAll(specialCharactersRegex, " ")
        .replace(/\s+/g, " ")
        .trim();
      const words = cleanName.split(" ");
  
      let remainingWordsNb = words.length;
      if (remainingWordsNb > maxLength) {
        remainingWordsNb = maxLength;
      }
  
      for (const word of words) {
        --remainingWordsNb;
  
        if (shortName.length >= maxLength) {
          break;
        } else {
          for (const letter of word) {
            //console.log('test:', word, letter, shortName, remainingWordsNb)
            if (shortName.length + remainingWordsNb >= maxLength) {
              break;
            } else {
              shortName += letter;
            }
          }
        }
      }
    }
  
    return shortName;
  }
  
  const res = companies.map((clientName) => [clientName, getShortName(clientName)]);
  
  res.forEach((res) => console.log(res[0], "=", res[1]));
  